//
//  Constants.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

struct Constants {

    struct NotificationKey {
        static let isNetworkReachable = "kIsNetworkReachable"
        static let networkNotReachable = "kNetworkNotReachable"
    }

    struct UserDefaults {
        static let previousResult = "kPreviousResult"
    }

}
