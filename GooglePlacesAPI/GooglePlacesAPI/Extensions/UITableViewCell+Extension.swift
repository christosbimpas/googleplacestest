//
//  UITableViewCell+Extension.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import UIKit.UITableViewCell

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
