//
//  UIViewController+Extension.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func navBarHeight() -> CGFloat {
        if let navBar = navigationController?.navigationBar {
            return navBar.frame.size.height
        } else {
            return 0
        }
    }
    
    func statusBarHeight() -> CGFloat {
        if let statusBarView = UIApplication.shared.statusBarView {
            return statusBarView.frame.height
        }
        return 0
    }
    
    func navBarStatusHeight() -> CGFloat {
        return navBarHeight() + statusBarHeight()
    }
    
    
    
    
}
