//
//  DataManager.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    var results: [[String:Any]]
    
    init(results: [[String:Any]]) {
        self.results = results
    }
    
    func searchResults() -> [SearchResult]? {
        let searchResults = results.map { (searchResultDict) -> SearchResult? in
            return SearchResult(JSON: searchResultDict)
        }
        let compactSearchResults = searchResults.compactMap { $0 } // remove nil values
        return compactSearchResults
    }
    
    
}

