//
//  GooglePlacesManager.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation


class GooglePlacesManager {
    
    let baseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    let apiKey = "AIzaSyDZoJmfyGSJeLuOaTMLWt6-2WCnYiA15vg"
    
    var location: String
    var radius: String
    var type: String?
    var keyword: String?
    
    init(location: String, radius: String) {
        self.location = location
        self.radius = radius
        
        
    }
    
    func makeRequestWithCompletion(_ completion:@escaping (_ searchResults: [SearchResult]?, _ error: NSError?) -> Void) {
        guard let path = requestPath() else {
            completion(nil, nil)
            return
        }
        print(path)
        let networkManager = NetworkManager(path: path)
        
        networkManager.simpleRequestWithCompletion { (result, error) in
            print(result ?? "")
            guard let result = result else {
                completion(nil, error)
                return
            }
            guard let resultDict = result as? [String:Any] else {
                completion(nil, error)
                return
            }
            guard let status = resultDict["status"] as? String else {
                completion(nil, error)
                return
            }
            if status == "OK" {
                guard let results = resultDict["results"] as? [[String:Any]] else {
                    completion(nil, error)
                    return
                }
                let dataManager = DataManager(results: results)
                completion(dataManager.searchResults(), error)
            } else {
                let error = NSError() //needs better error handling, not needed for this test I guess
                completion(nil, error )
            }
        }
    }
    
    func requestPath() -> String? {
        var path = baseUrl + "location=\(location)&radius=\(radius)&key=\(apiKey)"
        if let type = type {
            path = path + "&type=\(type)"
        }
        if let keyword = keyword {
            path = path + "&keyword=\(keyword)"
        }
        return path
    }
    
    func path() {
        
    }
    
    
    
    
}


