//
//  NetworkManager.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    
    var path: String!
    static let kPreviousResult = "kPreviousResult"
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    init(path: String) {
        self.path = path
    }
    
    func simpleRequestWithCompletion(_ completion:@escaping (_ result: Any?, _ error: NSError?) -> Void) {
        Alamofire.request(path).responseJSON { (response) in
            if let JSON = response.result.value  {
                self.saveResult(JSON)
                completion(JSON, nil)
            } else {
                //printresponse.result.error ?? "")
                completion(self.previousResult(), response.result.error as NSError?)
            }
        }
    }
    
    func previousResult() -> Any? {
        let userDefaults = UserDefaults.standard
        return userDefaults.value(forKey: NetworkManager.kPreviousResult)
    }
    
    func saveResult(_ result: Any?) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(result, forKey: NetworkManager.kPreviousResult)
        userDefaults.synchronize()
    }
    
    
    
    
}
