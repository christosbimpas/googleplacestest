//
//  Geometry.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import ObjectMapper

struct Geometry: Mappable {
   
    var location: Location?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        location <- map["location"]
        
        
    }
    
}
