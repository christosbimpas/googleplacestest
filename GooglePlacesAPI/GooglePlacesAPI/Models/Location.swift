//
//  Location.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import ObjectMapper

struct Location: Mappable {
    
    var lat: NSNumber?
    var lng: NSNumber?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
        
        
    }
    
}

