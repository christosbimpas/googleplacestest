//
//  SearchResult.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import ObjectMapper

struct SearchResult: Mappable {
    
    var id: String?
    var name: String?
    var icon: String?
    var types: [String?]?
    var vicinity: String?
    var geometry: Geometry?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        icon <- map["icon"]
        types <- map["types"]
        vicinity <- map["vicinity"]
        geometry <- map["geometry"]
        
    }
    
    func detailsText() -> String? {
        var text = ""
        if let name = name {
            text = "Name: \(name) "
        }
        if let vicinity = vicinity {
            text = text + "\nAddress: \(vicinity)"
        }
        if let types = types {
            let compactTypes = types.compactMap { $0 } // remove nil values
            let typesReduced = compactTypes.reduce("", {$0 + " " + $1})
            text = text + "\nTypes: \(typesReduced)"
        }
        print(text)
        return text
    }
    
}
