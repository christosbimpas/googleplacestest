//
//  DetailedScreenViewController.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class DetailedScreenViewController: UIViewController {
    
    var searchResult: SearchResult? {
        didSet {
            nameLabel.text = searchResult?.name
            formattedAddressLabel.text = searchResult?.vicinity
            guard let urlString = searchResult?.icon, let url = URL.init(string: urlString) else { return }
            iconImageView.sd_setImage(with: url, completed: nil)
           
        }
    }
    
    lazy var iconImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var formattedAddressLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        
        view.backgroundColor = .white
        
        view.addSubview(nameLabel)
        view.addSubview(formattedAddressLabel)
        view.addSubview(iconImageView)
        
        
        nameLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            make.left.right.equalToSuperview().inset(40)
        }
        
        formattedAddressLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(nameLabel.snp.bottom).offset(10)
            make.left.right.equalTo(nameLabel)
        }
        
        iconImageView.snp.makeConstraints { (make) in
            make.top.equalTo(formattedAddressLabel.snp.bottom).offset(20)
            
            make.centerX.equalToSuperview()
            make.width.height.equalTo(100)
        }
    }
}


