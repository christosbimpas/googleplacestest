//
//  MapViewController.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SnapKit

class MapViewController: UIViewController  {
    
    var searchResults: [SearchResult]? {
        didSet {
           addAnnotations()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        view.addSubview(mapView)
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func removePreviousAnnotations() {
        _ = mapView.annotations.map({mapView.removeAnnotation($0)})
    }
    
    private func addAnnotations() {
        guard let searchResults = searchResults else { return }
        removePreviousAnnotations()
        _ = searchResults.map { (searchResult) in
            let annotation = SearchResultAnnotation()
            annotation.title = searchResult.name
            annotation.searchResult = searchResult
            if let geometry = searchResult.geometry, let location = geometry.location, let lng = location.lng as? Double, let lat = location.lat as? Double {
                annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }
            mapView.addAnnotation(annotation)
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
    }
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.delegate = self
        mapView.isScrollEnabled = true
        return mapView
    }()
    
    let locationManager = CLLocationManager()
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
}

extension MapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let searchResultAnnotation = view.annotation as? SearchResultAnnotation else {
            return
        }
        let detailedScreenViewController = DetailedScreenViewController()
        detailedScreenViewController.searchResult = searchResultAnnotation.searchResult
        navigationController?.pushViewController(detailedScreenViewController, animated: true)
        
    }
}


