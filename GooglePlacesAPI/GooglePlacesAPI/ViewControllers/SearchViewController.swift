//
//  SearchViewController.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import UIKit

import Foundation
import UIKit
import SnapKit

enum DisplayType {
    case list
    case map
}

class SearchViewController: UIViewController {
    
    var searchResults: [SearchResult] = []
    
    var displayType: DisplayType = .list
    
        lazy var searchController: UISearchController = {
        let sc = UISearchController(searchResultsController: nil)
        sc.searchBar.placeholder = "Search for a place"
//        sc.searchBar.change(textFont: UIFont.lightFont(ofSize: 14))
        sc.searchBar.tintColor = .white
        sc.searchBar.backgroundColor = .black
        sc.searchResultsUpdater = self
        sc.searchBar.delegate = self
        sc.delegate = self
        sc.obscuresBackgroundDuringPresentation = false
        sc.searchBar.searchBarStyle = .prominent
        sc.hidesNavigationBarDuringPresentation = false
        return sc
    }()
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(SearchResultTableViewCell.self, forCellReuseIdentifier: SearchResultTableViewCell.identifier)
        tv.delegate = self
        tv.keyboardDismissMode = .onDrag
        tv.backgroundColor = .black
        tv.showsVerticalScrollIndicator = true
        tv.indicatorStyle = .white
        tv.separatorColor = UIColor.white.withAlphaComponent(0.12)
        tv.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        tv.dataSource = self
        return tv
    }()
    
    lazy var mapViewController: MapViewController = {
        let mapViewController = MapViewController()
        return mapViewController
    }()
    
    lazy var navTitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 1
        label.textColor = .white
        label.text = "Google Search API"
        return label
    }()
    
    lazy var rightBarButtonItem: UIBarButtonItem = {
        let button = UIBarButtonItem.init(title: "map", style: .plain, target: self, action: #selector(didTapOnBarButtonItem(_:)))
        return button
    }()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearance()
        setupViews()
        adjustDisplay()
        
        if !Connectivity.isConnectedToInternet() {
            updateResultsForKeyword("")
        }
    }
    
    private func setupAppearance() {
        view.backgroundColor = .black
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.searchController = searchController
        view.backgroundColor = .black
        navigationItem.titleView = navTitleLabel
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationItem.hidesSearchBarWhenScrolling = false
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white,NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 14)]
        UITextField.appearance().tintColor = .white
    }
    
    private func setupViews() {
        addChildViewController(mapViewController)
        
        view.addSubview(tableView)
        view.addSubview(mapViewController.view)
        
        tableView.snp.makeConstraints { (make) in
            make.bottom.right.left.equalToSuperview()
            make.top.equalToSuperview().offset(navBarStatusHeight())
        }
        
        mapViewController.view.snp.makeConstraints { (make) in
            make.edges.equalTo(tableView)
        }
    }
    
    private func adjustDisplay() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.tableView.alpha = self.displayType == .list ? 1.0 : 0.0
            self.mapViewController.view.alpha = self.displayType == .map ? 1.0 : 0.0
        }) { (isFinished) in
            if isFinished {
                if self.displayType == .map {
                    self.mapViewController.searchResults = self.searchResults
                }
            }
        }
        
        rightBarButtonItem.title = self.displayType == .map ? "list" : "map"
    }
    
    
    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.searchController.isActive = false
        }
    }
    
    @objc func didTapOnBarButtonItem(_ barButtonItem: UIBarButtonItem) {
        if barButtonItem == rightBarButtonItem {
            displayType = displayType == .list ? .map : .list
            adjustDisplay()
        }
    }
    
    func updateResultsForKeyword(_ keyword: String?) {
        let googlePlacesManager = GooglePlacesManager(location: "51.5150566,0.1020613", radius: "50000")
        googlePlacesManager.keyword = keyword
        googlePlacesManager.makeRequestWithCompletion { (searchResults, error) in
            guard let searchResults = searchResults else {
                return
            }
            self.searchResults = searchResults
            if self.displayType == .list {
                self.tableView.reloadData()
            } else {
                self.mapViewController.searchResults = searchResults
            }
            
        }
    }
}

extension SearchViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, text.count > 0 {
            updateResultsForKeyword(text)
        }
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SearchResultTableViewCell(style: .default, reuseIdentifier: SearchResultTableViewCell.identifier)
        cell.searchResult = searchResults[indexPath.row]
        return cell
    }
}

extension SearchViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = SearchResultTableViewCell(style: .default, reuseIdentifier: SearchResultTableViewCell.identifier)
        return cell.height(searchResults[indexPath.row])
    }
}

