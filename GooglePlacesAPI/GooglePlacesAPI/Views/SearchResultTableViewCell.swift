//
//  SearchResultTableViewCell.swift
//  GooglePlacesAPI
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import SDWebImage


class SearchResultTableViewCell: UITableViewCell {
    
    
    var searchResult: SearchResult? {
        didSet {
            detailsLabel.text = searchResult?.detailsText()
            guard let iconUrl = searchResult?.icon, let url = URL.init(string: iconUrl) else {
                return
            }
            iconImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
    lazy var iconImageView: UIImageView = {
        let iconImageView = UIImageView()
        iconImageView.contentMode = .scaleAspectFill
        return iconImageView
    }()
    
    lazy var detailsLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
        backgroundColor = .white
        setupViews()
    }
    
    private func setupViews() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(detailsLabel)
       
        iconImageView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(margin)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(iconWidthHeight)
        }
        
        detailsLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(iconImageView)
            make.left.equalTo(iconImageView.snp.right).offset(margin)
            make.right.bottom.top.equalToSuperview().inset(margin)
        }
        
    }
    
    let margin: CGFloat = 10
    let iconWidthHeight: CGFloat = 50
    
    func height(_ searchResult: SearchResult?) -> CGFloat {
        guard let detailsText = searchResult?.detailsText() else {
            return iconWidthHeight + margin * 2
        }
        return detailsText.height(withConstrainedWidth: contentView.frame.size.width - margin * 3 - iconWidthHeight, font: detailsLabel.font) + margin * 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
