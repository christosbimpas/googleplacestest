//
//  GooglePlacesAPITests.swift
//  GooglePlacesAPITests
//
//  Created by Christos Bimpas on 25/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import XCTest
@testable import GooglePlacesAPI

class GooglePlacesAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_NetworkManager() {
        let networkManager = NetworkManager(path: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=1500&type=pub&key=AIzaSyDZoJmfyGSJeLuOaTMLWt6-2WCnYiA15vg")
        networkManager.simpleRequestWithCompletion { (result, error) in
            XCTAssert(result != nil && error == nil)
        }
    }
    
    func test_GooglePlacesManagerResultsNotNil() {
        let googlePlacesManager = GooglePlacesManager(location: "51.5150566,0.1020613", radius: "50000")
        googlePlacesManager.keyword = "pizza"
        googlePlacesManager.makeRequestWithCompletion { (result, error) in
            XCTAssert(result != nil && error == nil)
        }
    }
    
    func test_GooglePlacesManagerResultsNotZero() {
        let googlePlacesManager = GooglePlacesManager(location: "51.5150566,0.1020613", radius: "50000")
        googlePlacesManager.keyword = "pizza"
        googlePlacesManager.makeRequestWithCompletion { (searchResults, error) in
            XCTAssert(searchResults!.count > 0)
        }
    }
    
   
    
}
