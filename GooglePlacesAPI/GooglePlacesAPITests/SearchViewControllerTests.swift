//
//  SearchViewControllerTests.swift
//  GooglePlacesAPITests
//
//  Created by Christos Bimpas on 26/04/2018.
//  Copyright © 2018 Christos Bimpas. All rights reserved.
//

import XCTest
@testable import GooglePlacesAPI

class SearchViewControllerTests: XCTestCase {
    
    var viewController: SearchViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController = SearchViewController()
        _ = viewController.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewController = nil
        super.tearDown()
    }
    
   
    func testVC_TableViewShouldNotBeNil() {
        XCTAssertNotNil(viewController.tableView)
    }
    
    
    func testDataSource_ViewDidLoad_SetsTableViewDataSource() {
        XCTAssertNotNil(viewController.tableView.dataSource)
    }
    

    func testDelegate_ViewDidLoad_SetsTableViewDelegate() {
        XCTAssertNotNil(viewController.tableView.delegate)
    }
    
    func testVC_SearchControllerShouldNotBeNil() {
        XCTAssertNotNil(viewController.searchController)
    }
    
}
